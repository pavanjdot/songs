import React from 'react'
import SongList from './SongList'
import SongsDetails from './SongDetails'



class App extends React.Component{
    
    render(){
        
        return(
            <div className='ui container grid'>
                <div className='ui row'>
                    <div className='column eight wide'>
                             <SongList/>
                    </div>
                    <div className='column eight wide'>
                        <SongsDetails/>

                    </div>

                </div>
                
                
            </div>
        );
    }

}
    

export default App;

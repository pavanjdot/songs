import {combineReducers} from 'redux';

export const songsReducer = () =>{
    return[
        {title: 'No Scrubs', duration: '4:05'},
        {title: 'Ghandhi Money', duration: '3:45'},
        {title: 'Remand', duration: '2:45'},
        {title: 'kohinor', duration: '1:30'}
    ];

};

export const slectedSongReducer = (selectedSong = null, action)=> {
    if(action.type === 'SONG_SELECTED'){
        return action.payload;
    }

    return selectedSong;

}

export default combineReducers({
    songs: songsReducer,
    selectedSong: slectedSongReducer    
});